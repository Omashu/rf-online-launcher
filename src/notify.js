let queue = null;
export const setQueue = (value) => {
	queue = value;
}

export const showInQueue = (value, type, tm, color) => {
	if (!queue) return null;
	return queue(value,type,tm,color);
}

export default showInQueue;