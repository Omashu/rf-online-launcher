import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'

import moment from 'moment';
moment.locale('ru');

import App from './components/App.jsx';

// create global store
import reducers from './store/reducers'
import { createStore } from 'redux'

const store = createStore(reducers, {},
	window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

window.onload = function() {
	ReactDOM.render((
		<Provider store={store}>
			<App/>
		</Provider>
	), document.getElementById('appMount'));
};