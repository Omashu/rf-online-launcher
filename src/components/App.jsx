import React from 'react';
import { connect } from 'react-redux';

import Layout from './Layout.jsx';
import Sidebar from './containers/sidebar/Index.jsx';
import Content from './containers/content/Index.jsx';

class App extends React.Component {
	constructor(props) {
		super(props);
	}
	componentWillMount() {
	}
	render() {
		return (
			<Layout id="wrapper">
				<Sidebar/>
				<Content/>
			</Layout>
		);
	}
}

export default connect(
	(state, ownProps) => ({
		appIsLoading : state.app.loading,
	})
)(App);