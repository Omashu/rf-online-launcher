import React from 'react';
import { Field, reduxForm } from 'redux-form'
import cx from 'classnames'
import random from 'randomstring';

import { FormInput } from './FormFields.jsx'

const validate = values => {
	const errors = {};

	if (!values.name) errors.name = 'Введите логин.';
	else if (values.name.length < 4)
		errors.name = 'Минимальная длина логина 4 символов. Вы ввели: '+values.name.length;
	else if (values.name.length > 12)
		errors.name = 'Максимальная длина логина 12 символов. Вы ввели: '+values.name.length;
	else if (/[^a-z0-9]+/i.test(values.name))
		errors.name = 'Допустимые символы в логине [a-z0-9]';

	return errors;
};

class Component extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		const { handleSubmit, pristine, reset, submitting, error, invalid, submitFailed, onClickCancel, form } = this.props;

		return (
			<div className={cx("form__createAccount", "form__createAccount-" + form)}>

				<div className="form-group">
					{error && <p className="alert alert-danger">{error}</p>}
				</div>

				<Field
					name="name"
					type="text"
					component={FormInput}/>


				<div className="form-group">
					<button disabled={pristine || submitting || (invalid && !submitFailed)}
						type="button" className="btn btn-success btn-sm" onClick={handleSubmit}>
						{submitting && <span><i className="fa fa-spinner fa-spin fa-fw"></i> Загрузка</span>}
						{!submitting && <span><i className="fa fa-send fa-fw"></i> Создать</span>}
					</button>
					&nbsp;
					<button type="button" onClick={onClickCancel} className="btn btn-sm btn-light">Отмена</button>
				</div>
			</div>
		);
	}
}

export default reduxForm({
	validate,
})(Component)