import React from 'react';
import { Field, reduxForm } from 'redux-form'
import cx from 'classnames'

import { FormInput } from './FormFields.jsx'

const validate = values => {

};

const FormLogin = (props) => {
	const { handleSubmit, pristine, reset, submitting, error, invalid, submitFailed } = props

	return (
		<div className={cx("form__login", "form__login-" + props.form)}>
			<h1 className="header">Активация</h1>

			<div className="form-group">
				{error && <p className="alert alert-danger">{error}</p>}
			</div>

			<Field
				name="key"
				type="text"
				component={FormInput}
				placeholder="XX-XX-XX"
				label="Код активации (отправлен на ваш e-mail адрес)"/>

			<div className="form-group">
				<button disabled={pristine || submitting || (invalid && !submitFailed)}
					type="button" className="btn btn-success btn-sm" onClick={handleSubmit}>
					{submitting && <span><i className="fa fa-spinner fa-spin fa-fw"></i> Загрузка</span>}
					{!submitting && <span><i className="fa fa-send fa-fw"></i> Войти</span>}
				</button>
			</div>
		</div>
	)
}

export default reduxForm({
	validate,
})(FormLogin)