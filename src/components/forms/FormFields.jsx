import React from 'react';
import cx from 'classnames'

export const FormInput = ({
	input,
	label,
	labelHelp = null,
	type,
	addon,
	iconFa=true,
	iconRight,
	iconRightTrue,
	iconRightFalse,
	placeholder,
	help,
	meta : {
		touched,
		error
	}
}) =>
	<div className={cx('form-group')}>
		{label && <label>{label}</label>}
		{labelHelp && <div className="label__desc">{labelHelp}</div>}

		<input {...input} placeholder={placeholder} type={type} className={cx('form-control',
			{'is-invalid': (touched&&error)},
			{'is-valid':(touched&&!error)})} />

		{(touched && error || help) &&
			<span className={'invalid-feedback ' + (touched&&error?'is-danger':'')}>
				{(touched&&error)?error:help}
			</span>}
	</div>