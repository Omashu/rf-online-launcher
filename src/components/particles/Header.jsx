import React from 'react';
import { connect } from 'react-redux';
import cx from 'classnames';

const remote = require('electron').remote;
import { title as appTitle } from '../../config';

class ParticleHeader extends React.Component {
	handleOnClickQuit() {
		const window = remote.getCurrentWindow();
		window.close();
	}
	render() {
		return (<nav className="navbar">
			<div className="navbar-overlay"></div>
			<div className="navbar-brand" title={appTitle}>
				<span className={cx({
					"circle" : true,
					"bg-success" : this.props.bConnectConnected,
					"bg-danger" : !this.props.bConnectConnected
				})}></span>
				{appTitle || "Project"}
				<span className={cx({
					"connect" : true,
					"text-danger" : !this.props.bConnectConnected && this.props.connectLastECode,
					"text-success" : this.props.bConnectConnected
				})}>
					{!this.props.bConnectConnected && this.props.connectLastECode &&
						<span>{this.props.connectLastECode === 'ECONNREFUSED'
							? 'Логин сервер не отвечает' : 'Соединение потеряно'}</span>
					}
					{this.props.bConnectConnected &&
						<span>Соединение установлено</span>
					}
					{this.props.bConnectConnection && !this.props.bConnectConnected &&
						<i className="fa fa-spinner fa-pulse fa-fw"/>
					}
				</span>
			</div>
			<div className="navbar-controls">
				<button className="btn" onClick={this.handleOnClickQuit}><i className="fa fa-fw fa-times"/></button>
			</div>
		</nav>);
	}
}

export default connect(
	(state, ownProps) => ({
		appIsLoading : state.app.loading,
		bConnectConnected : state.connect.connected,
		bConnectConnection : state.connect.connection,
		connectLastECode : state.connect.lastECode,
	})
)(ParticleHeader);