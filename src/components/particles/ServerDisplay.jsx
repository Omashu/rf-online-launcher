import React from 'react';
import { connect } from 'react-redux';
import cx from 'classnames';

class Component extends React.Component {
	render() {
		return (<div className="block block__serverDisplay">
			<div className="row">
				<div className="col">
					<span className="prefix">A:</span>
					<span className="value">{this.props.display.user
						? this.props.display.user.a : 0}</span>
				</div>
				<div className="col">
					<span className="prefix">B:</span>
					<span className="value">{this.props.display.user
						? this.props.display.user.b : 0}</span>
				</div>
				<div className="col">
					<span className="prefix">C:</span>
					<span className="value">{this.props.display.user
						? this.props.display.user.c : 0}</span>
				</div>
			</div>
		</div>);
	}
}

export default connect(
	(state, ownProps) => ({
		display : state.serverDisplay
	})
)(Component);