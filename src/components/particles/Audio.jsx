import React from 'react';
import { connect } from 'react-redux';
import cx from 'classnames';
import _ from 'lodash';

import { write as writeStorage, get as getStorage } from '../../storage';
import { audioTrackList } from '../../config';

class Component extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			current : null,
			play : false
		};
	}
	getRandomTrack() {
		const count = audioTrackList.length;

		if (!count)
			return null;

		const i = Math.floor(Math.random() * count-1) + 1;
		audioTrackList[i]._key = i;
		return audioTrackList[i];
	}
	componentWillMount() {
		const track = this.getRandomTrack();
		this.setState({
			current : track,
			play : track ? !!this.props.audioAutoPlay : false,
		});
	}
	handleOnClickNext = (el) => {
		if (!this.state.current)
			return null;

		let nextKey = this.state.current._key + 1;

		if (!audioTrackList[nextKey])
			nextKey = 0;

		audioTrackList[nextKey]._key = nextKey;

		this.setState({
			play : true,
			current : audioTrackList[nextKey]
		});
	}
	handleOnClickPrev = (el) => {
		if (!this.state.current)
			return null;

		let prevKey = this.state.current._key - 1;

		if (prevKey < 0 || !audioTrackList[prevKey])
			prevKey = audioTrackList.length-1;

		audioTrackList[prevKey]._key = prevKey;

		this.setState({
			play : true,
			current : audioTrackList[prevKey]
		});
	}
	handleOnClickToggle = (el) => {
		if (!this.state.current)
			return null;

		writeStorage(_.extend({}, getStorage(), {
			audioAutoPlay : !this.state.play
		}));

		this.setState({
			...this.state,
			play : !this.state.play
		});
	}
	handeOnEnded = (el) => {
		// yo!
		this.handleOnClickNext();
	}
	render() {
		return (<div className="block block__audio">
			<div className="container" style={{minWidth:300}}>
				<div className="row">
					<div className="col">
						<div className="name">{[this.state.current.executor, this.state.current.name].join(" - ")}</div>
					</div>
					<div className="col mr-auto">
						<div className="controls">
							<div className="btn-group" role="group">
								<button onClick={this.handleOnClickPrev} type="button" className="btn btn-secondary btn-sm">
									<i className="fa fa-fast-backward" aria-hidden="true"></i></button>
								<button type="button" className="btn btn-secondary btn-sm" onClick={this.handleOnClickToggle}>
									{this.state.play && this.state.current &&
										<i className="fa fa-stop" aria-hidden="true"></i>
									}
									{(!this.state.play || !this.state.current) &&
										<i className="fa fa-play" aria-hidden="true"></i>
									}
								</button>
								<button onClick={this.handleOnClickNext} type="button" className="btn btn-secondary btn-sm">
									<i className="fa fa-fast-forward" aria-hidden="true"></i></button>
							</div>
						</div>
					</div>
				</div>
			</div>
			{this.state.play && this.state.current &&
				<audio
					onEnded={this.handeOnEnded}
					autoPlay={this.state.play}
					controls={false}
					src={this.state.current.path+this.state.current.filename}></audio>
			}
		</div>);
	}
}

export default connect(
	(state, ownProps) => ({
		appIsLoading : state.app.loading,
		audioAutoPlay : state.storage.audioAutoPlay
	})
)(Component);