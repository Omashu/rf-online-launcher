import React from 'react';
import { connect } from 'react-redux';
import Notifications, {notify} from 'react-notify-toast';
import { setQueue } from '../notify.js';

import Header from './particles/Header.jsx';
import MainLoader from './containers/MainLoader.jsx';

class Layout extends React.Component {
	constructor(props) {
		super(props);
	}
	componentWillMount() {
		setQueue(notify.createShowQueue());
	}
	render() {
		return (<div id={this.props.id}>
			<Notifications options={{zIndex: 1052}} />
			<Header/>
			<MainLoader/>
			{!this.props.appIsLoading && this.props.children}
		</div>);
	}
}

export default connect(
	(state, ownProps) => ({
		appIsLoading : state.app.loading
	}),
	dispatch => ({})
)(Layout);