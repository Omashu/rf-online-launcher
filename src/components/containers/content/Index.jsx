import React from 'react';
import { connect } from 'react-redux';
import cx from 'classnames';
import Promise from 'bluebird';
import _ from 'lodash';

import { pageIndex } from '../../../action';
import Audio from '../../particles/Audio.jsx';
import ServerDisplay from '../../particles/ServerDisplay.jsx';
import socket from '../../../socket';
const shell = require('electron').shell;

class Component extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			loading : true,
			error : null,
			pages : []
		};
	}
	componentWillMount() {
		pageIndex({
			order : {col : "createdAt",way : "desc"},
			limit : 10
		})
		.asCallback((err, data) => {
			if (!err && data.error)
				err = new Error(data.error);

			if (err)
				return this.setState({
					error : err.message,
					loading : false
				})

			return this.setState({
				pages : data.result.results,
				loading : false
			})
		})

		socket().on("pages_created", this.handleCreated);
		socket().on("pages_updated", this.handleUpdated);
	}
	componentWillUnmount() {
		socket().off("pages_created", this.handleCreated);
		socket().off("pages_updated", this.handleUpdated);
	}
	handleCreated = (data) => {
		const pages = _.extend([], this.state.pages);
		pages.unshift(data);

		this.setState({
			pages
		});
	}
	handleUpdated = (data) => {
		const pages = _.extend([], this.state.pages);
		const findIndex = _.findIndex(pages, userAccount => userAccount.id === data.id);
		if (findIndex === -1) return this.handleCreated(data);

		pages[findIndex] = _.extend({}, pages[findIndex], data);

		this.setState({
			pages
		});
	}
	render() {
		return (
			<div className="content">
				<div className="container-fluid no-gutters">
					<div className="row">
						<div className="col"><ServerDisplay/></div>
						<div className="col"><Audio/></div>
					</div>
				</div>

				<div className="content__pages">
					<div className="container">
						<div className="row">
							<div className="col">
								{this.state.error &&
									<div className="alert alert-danger">
										{this.state.error}
									</div>
								}
								{this.state.loading &&
									<div style={{textAlign:'center'}}>
										<i className="fa fa-spin fa-spinner fa-3x"/>
									</div>
								}
								{this.state.pages.map(page => 
									<div className="card" key={page.id}>
										<div className="card-body">
											<h4 className="card-title">
												<a onClick={(event) => {
													event.preventDefault();
													shell.openExternal(event.target.href);
												}} href={page.url} title={page.title}>{page.title}</a>
											</h4>
											<div className="card-text" dangerouslySetInnerHTML={{__html: page.shortContent}}/>
										</div>
									</div>
								)}
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default connect(
	(state, ownProps) => ({
		authUser : state.auth.ur
	})
)(Component);