import React from 'react';
import { connect } from 'react-redux';
import cx from 'classnames';
import Promise from 'bluebird';
import _ from 'lodash';
import fs from 'fs';

import * as dispatchTypes from '../../store/types';
import { connect as SocketConnect, setToken as SocketSetToken } from '../../socket';
import { setCurrent as setCurrentStorage } from '../../storage';
import { connectionUrl } from '../../config';

class Component extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			loading : true,
			bError : false,
			message : null,
			subMessage : null
		};

		this.StoragePath = "LauncherStorage.json";
	}
	componentWillMount() {
		(new Promise((resolve, reject) => {
			fs.readFile(this.StoragePath, (err, data) => {
				// ignore
				if (err && err.code === "ENOENT")
					return resolve({});

				if (err)
					return reject(err);

				return resolve(JSON.parse(data.toString()||JSON.stringify({})) || {});
			});
		}))
		.then(storageData => {
			if (!storageData)
				throw new Error("Could not read storage");

			if (typeof storageData !== "object")
				throw new Error("Some error in the storage");

			if (!connectionUrl)
				throw new Error("Connection url not provided");

			// dispatch storage values
			_.forEach(storageData, (value, key) => {
				let upper = key.toUpperCase();
				this.props.dispatch({
					type : `STORAGE_${upper}`,
					payload : value
				});
			});

			setCurrentStorage(storageData);
			return storageData;
		})
		.then(storageData => {
			// create socket connection
			if (storageData["userAccounts"] && storageData["userAccounts"].length === 1)
				SocketSetToken(storageData["userAccounts"][0].token);

			SocketConnect(connectionUrl, {}, this.props.dispatch);
			return storageData;
		})
		.then(storageData => {
			// process complete
			this.setState({
				loading : false,
				bError : false,
				message : null,
				subMessage : null
			});

			this.props.dispatch({
				type : dispatchTypes.APP_LOADING,
				payload : false
			});
		})
		.catch(err => {
			// process error
			this.setState({
				loading : false,
				bError : true,
				message : 'Something Error',
				subMessage : err.message + (err.code?` (${err.code})`:""),
			});
		});
	}
	render() {
		if (!this.props.appIsLoading)
			return null;

		return (<div className="part__main_loader">
			<div className="overlay"></div>
			<div className="box">
				<div className={cx({
					"box_ico" : true,
					"text-success" : !this.state.bError && !this.state.loading
				})}>
					<i className={cx({
						"fa fa-fw fa-3x" : true,
						"fa-spinner fa-spin" : this.state.loading,
						"fa-times" : this.state.bError,
						"fa-check" : !this.state.bError && !this.state.loading
					})}/>
				</div>
				{this.state.message && <div className="box_msg">{this.state.message}</div>}
				{this.state.subMessage && <div className="box_submsg">{this.state.subMessage}</div>}
			</div>
				</div>);
	}
}

export default connect(
	(state, ownProps) => ({
		appIsLoading : state.app.loading,
		authUser : state.auth.ur
	})
)(Component);