import React from 'react';
import { connect } from 'react-redux';
import cx from 'classnames';
import Promise from 'bluebird';
import _ from 'lodash';

import Authorized from './Authorized.jsx';
import Authorize from './Authorize.jsx';
import Footer from './Footer.jsx';

class Component extends React.Component {
	constructor(props) {
		super(props);
	}
	componentWillMount() {
	}
	render() {
		return (
			<aside className="sidebar">
				{this.props.authUser &&
					<Authorized/>
				}

				{!this.props.authUser &&
					<Authorize/>
				}

				<footer>
					<Footer/>
				</footer>
			</aside>
		);
	}
}

export default connect(
	(state, ownProps) => ({
		authUser : state.auth.ur
	})
)(Component);