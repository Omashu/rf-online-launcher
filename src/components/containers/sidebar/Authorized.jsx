import React from 'react';
import { connect } from 'react-redux';
import cx from 'classnames';
import Promise from 'bluebird';
import _ from 'lodash';
import moment from 'moment';

import notify from '../../../notify';
import Accounts from './Accounts.jsx';
import AccountCreate from './AccountCreate.jsx';

class Component extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			createAccount : false
		};
	}
	handleOnClickCreate = () => {
		this.setState({
			createAccount : true
		})
	}
	handleOnClickCancel = () => {
		this.setState({
			createAccount : false
		})
	}
	componentWillMount() {

	}
	render() {
		const { authUser } = this.props;

		return (
			<div className="sidebar__authorized">
				{authUser.isBanned &&
					<div className="alert alert-danger" style={{margin:25}}>
						{authUser.bannedTo && <span>Доступ к аккаунту ограничен до: <b>{moment(authUser.bannedTo).format('LLL')}</b>.</span>}
						{!authUser.bannedTo && <span>Аккаунт навсегда заблокирован.</span>}
						&nbsp;Дата блокировки: <b>{moment(authUser.bannedFrom).format('LLL')}</b>
						{authUser.banReason &&
							<div>
								<hr/>
								{authUser.banReason}
							</div>
						}
					</div>
				}

				{!authUser.isBanned && !this.state.createAccount &&
					<Accounts onClickCreate={this.handleOnClickCreate}/>
				}

				{!authUser.isBanned && this.state.createAccount &&
					<AccountCreate onClickCancel={this.handleOnClickCancel}/>
				}
			</div>
		);
	}
}

export default connect(
	(state, ownProps) => ({
		authUser : state.auth.ur
	})
)(Component);