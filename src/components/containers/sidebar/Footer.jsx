import React from 'react';
import { connect } from 'react-redux';
import { SubmissionError } from 'redux-form';
import cx from 'classnames';
import fs from 'fs';
import async from 'async';
import Promise from 'bluebird';
import _ from 'lodash';

import notify from '../../../notify';
import { write as writeStorage } from '../../../storage';
import socket, { setToken, reconnect } from '../../../socket';
import { connectionUrl, forumUrl } from '../../../config';
import { getToken, activateToken, userAccountsIndexByUserId, createSession } from '../../../action';

const { exec } = require('child_process');
const shell = require('electron').shell;

class Component extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			configureInProcess : false
		};
	}
	componentWillMount() {
	}
	componentWillUnmount() {
	}
	handleConfigureR3Engine = () => {
		if (this.state.configureInProcess)
			return;

		this.setState({
			configureInProcess : true
		});

		exec("Setting.exe", (err) => {
			this.setState({
				configureInProcess : false,
			});

			if (err)
				return notify("Неудалось запустить Setting.exe", "error");

			return notify("Параметры запуска обновлены", "success");
		});
	}
	render() {
		return (
			<div className="sidebar__footer" style={{textAlign:'center'}}>
				<hr/>
				<a onClick={(event) => {
					event.preventDefault();
					shell.openExternal(event.target.href);
				}} href={forumUrl} className="btn btn-info" style={{marginRight:10}}><i className="fa fa-fw fa-comments"/> Форум</a>
				<button disabled={this.state.configureInProcess} type="button" onClick={this.handleConfigureR3Engine} className="btn btn-info"><i className="fa fa-fw fa-cog"/> Параметры запуска</button>
			</div>
		);
	}
}

export default connect(
	(state, ownProps) => ({
		authUser : state.auth.ur,
		isConnected : state.connect.connected
	})
)(Component);