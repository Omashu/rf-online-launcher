import React from 'react';
import { connect } from 'react-redux';
import { SubmissionError } from 'redux-form';
import cx from 'classnames';
import Promise from 'bluebird';
import _ from 'lodash';

import * as dispatchTypes from '../../../store/types';
import { write as writeStorage, get as getStorage } from '../../../storage';
import socket, { setToken, reconnect } from '../../../socket';
import { getToken, activateToken } from '../../../action';
import { connectionUrl } from '../../../config';
import notify from '../../../notify';

import FormLogin from '../../forms/Login.jsx';
import FormTokenActivation from '../../forms/TokenActivation.jsx';

class Component extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			activation : false,
			tokenData : null,
			waitReconnect : false
		};

		this.lastIdent = null;
	}
	componentWillMount() {
	}
	dispatchAuthorized = (token) => {
		setToken(token);
		
		// try reconnect
		reconnect(connectionUrl, {}, this.props.dispatch);

		const userAccounts = _.extend([], getStorage().userAccounts || []);
		let userAccount = userAccounts.find(userAccount => userAccount.ident === this.lastIdent);

		if (userAccount) {
			userAccount.token = token;
		} else {
			userAccount = {
				ident : this.lastIdent,
				token : token
			};
			userAccounts.push(userAccount);
		}

		const newStorage = _.extend({}, getStorage(), {
			userAccounts
		});

		writeStorage(newStorage);

		this.props.dispatch({
			type : dispatchTypes.STORAGE_USERACCOUNTS,
			payload : userAccounts
		});

		return this.setState({
			waitReconnect : true
		});
	}
	handleSubmit = (values) => {
		this.lastIdent = values.ident;
		return getToken(values)
			.then(data => {
				if (data.error)
					throw new SubmissionError({_error:data.error + ` (${data.code})`});

				if (!data.result.isActive && !data.result.confirmed)
					return this.setState({
						activation : true,
						tokenData : data.result
					});

				return this.dispatchAuthorized(data.result.token);
			})
			.catch(err => {
				if (err instanceof SubmissionError)
					throw err;

				throw new SubmissionError({_error:err.message});
			});
	}
	handleSubmitTokenActivation = (values) => {
		return activateToken({...values, token : this.state.tokenData.token})
			.then(data => {
				if (data.error)
					throw new SubmissionError({_error:data.error + ` (${data.code})`});

				return this.dispatchAuthorized(this.state.tokenData.token);
			})
			.catch(err => {
				if (err instanceof SubmissionError)
					throw err;

				throw new SubmissionError({_error:err.message});
			});
	}
	handleSelectUserAccount = (userAccount) => {
		this.setState({
			waitReconnect : true
		})

		setToken(userAccount.token);
		reconnect(connectionUrl, {}, this.props.dispatch);
		socket().once("ur", (data) => {
			// сервер ответил пользователем
			if (data)
				return;

			// токен умер или что-то неправильно, удаляем из стека
			const userAccounts = _.extend([], getStorage().userAccounts || []);
			const newUserAccounts = [];

			_.forEach(userAccounts, value => {
				if (value.ident !== userAccount.ident)
					newUserAccounts.push(value);
			})

			const newStorage = _.extend({}, getStorage(), {
				userAccounts : newUserAccounts
			});

			writeStorage(newStorage);

			this.props.dispatch({
				type : dispatchTypes.STORAGE_USERACCOUNTS,
				payload : newUserAccounts
			});

			notify("Токен доступа устарел или владелец аккаунта ограничил доступ. Авторизуйтесь снова.", "error");

			this.setState({
				waitReconnect : false
			})
		});
	}
	render() {
		return (
			<div className="sidebar__authorize">
				{!this.state.waitReconnect &&
					<div>
						{this.props.userAccounts instanceof Array && this.props.userAccounts.length > 0 && !this.state.activation &&
							<div className="block">
								<h1 className="header">Выбор учетной записи</h1>
								<div className="block__body" style={{padding:'0 20px'}}>
									{this.props.userAccounts.map(userAccount => 
										<a onClick={this.handleSelectUserAccount.bind(null,userAccount)} style={{margin:'5px 5px'}} key={userAccount.ident} className="btn btn-link btn-light btn-sm">
											{userAccount.ident}
										</a>
									)}
								</div>
							</div>
						}
						{!this.state.activation &&
							<FormLogin
								form="sidebarAuthorize"
								onSubmit={this.handleSubmit}
							/>
						}
						{this.state.activation &&
							<FormTokenActivation
								form="sidebarAuthorizeTokenActivation"
								onSubmit={this.handleSubmitTokenActivation}
							/>
						}
					</div>
				}

				{this.state.waitReconnect &&
					<div style={{
						padding : '25px',
						textAlign : 'center',
						fontSize : '22px'
					}}><i className="fa fa-spin fa-spinner fa-3x"/></div>
				}
			</div>
		);
	}
}

export default connect(
	(state, ownProps) => ({
		authUser : state.auth.ur,
		userAccounts : state.storage.userAccounts
	})
)(Component);