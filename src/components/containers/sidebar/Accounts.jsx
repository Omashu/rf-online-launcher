import React from 'react';
import { connect } from 'react-redux';
import { SubmissionError } from 'redux-form';
import cx from 'classnames';
import fs from 'fs';
import async from 'async';
import Promise from 'bluebird';
import _ from 'lodash';
import path from 'path';

import notify from '../../../notify';
import { write as writeStorage } from '../../../storage';
import socket, { setToken, reconnect } from '../../../socket';
import { connectionUrl } from '../../../config';
import { getToken, activateToken, userAccountsIndexByUserId, createSession } from '../../../action';

const { exec, spawn, execFile } = require('child_process');

const queueLaunch = async.queue((task, cb) => {
	setTimeout(() => {
		const DefaultSetPath = path.resolve('../System/DefaultSet.tmp');
		const RFOnlineBinPath = path.resolve("../RF_Online.bin");
		const RFOnlineRoot = path.resolve("../");

		fs.open(DefaultSetPath, "w+", (err, fd) => {
			if (err) {
				return cb(err);
			}

			fs.write(fd, task.defaultSet, 0, task.defaultSet.length, null, (err) => {
				if (err) {
					return cb(err);
				}

				fs.close(fd, (err) => {
					// запускаем игру
					if (!err)
					{
						execFile(RFOnlineBinPath, {
							cwd : RFOnlineRoot
						}, task.onExecutionOut);
					}

					// "завершаем" таск, говорим что начали запуск игры
					cb(err);
				});
			});
		});
	}, 1500);
});

class Component extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			loading : true,
			userAccounts : [],
			error : null,
			userAccountProcess : {},
		};
	}
	componentWillMount() {
		userAccountsIndexByUserId(this.props.authUser.id)
		.asCallback((err, data) => {
			if (!err && data.error)
				err = new Error(data.error);

			if (err)
				return this.setState({
					error : err.message,
					loading : false
				})

			this.setState({
				userAccounts : data.result.results,
				loading : false
			})
		});

		socket().on("userAccounts_created", this.handleCreated);
		socket().on("userAccounts_updated", this.handleUpdated);
	}
	componentWillUnmount() {
		socket().off("userAccounts_created", this.handleCreated);
		socket().off("userAccounts_updated", this.handleUpdated);
	}
	onClickCreate = (el) => {
		return this.props.onClickCreate && this.props.onClickCreate(el);
	}
	onClickLogout = (el) => {
		setToken(null);
		this.setState({
			loading : true,
			userAccounts : [],
			error : null,
			userAccountProcess : {}
		});
		reconnect(connectionUrl, {}, this.props.dispatch);
	}
	handleCreated = (data) => {
		if (this.props.authUser.id !== data.userId)
			return;

		const userAccounts = _.extend([], this.state.userAccounts);
		userAccounts.unshift(data);

		this.setState({
			userAccounts
		});
	}
	handleUpdated = (data) => {
		if (this.props.authUser.id !== data.userId)
			return;

		const userAccounts = _.extend([], this.state.userAccounts);
		const findIndex = _.findIndex(userAccounts, userAccount => userAccount.id === data.id);
		if (findIndex === -1) return;

		userAccounts[findIndex] = _.extend({}, userAccounts[findIndex], data);

		this.setState({
			userAccounts
		});
	}
	setStateProcess = (id, data) => {
		const process = Object.assign({}, this.state.userAccountProcess[id] || {}, data);

		const newStateProcess = _.extend({}, this.state.userAccountProcess || {});
		newStateProcess[id] = process;

		this.setState({
			userAccountProcess : newStateProcess
		});
	}
	handleOnClickLaunch = (userAccount) => {
		if (!this.props.isConnected)
			return;

		this.setStateProcess(userAccount.id, {
			running : true,
			execution : false
		});
		
		createSession({
			userAccountId : userAccount.id
		})
		.asCallback((err, data) => {
			if (!err && data.error)
				err = new Error(data.error);

			if (err) {
				this.setStateProcess(userAccount.id, {
					running : false,
					execution : false
				})

				return notify(`${userAccount.name}: ${err.message}`, "error");
			}

			queueLaunch.push({
				defaultSet : data.result.defaultSet,
				onExecutionOut : (err) => {
					// выполняется при закрытии игры
					this.setStateProcess(userAccount.id, {
						running : false,
						execution : false
					});

					if (err) {
						return notify(`${userAccount.name}: ${err.message}`, "error")
					}
				}
			}, (err) => {
				// выполняется при окончании записи и начале запуска игры
				if (err) {
					notify(`${userAccount.name}: ${err.message}`, "error")
					return this.setStateProcess(userAccount.id, {
						running : false,
						execution : false
					});
				}

				this.setStateProcess(userAccount.id, {
					running : false,
					execution : true
				});
			});
		});
	}
	render() {
		return (
			<div className="sidebar__accounts">
				<div className="block block__accounts">
					<h1 className="header">
						Выберите игровой аккаунт
					</h1>

					{this.state.loading &&
						<div style={{textAlign:'center'}}><i className="fa fa-spinner fa-spin fa-fw fa-3x"/></div>
					}
					{this.state.error &&
						<div style={{textAlign:'center',color:'red'}}>{this.state.error}</div>
					}
					{!this.state.loading && !this.state.error && this.state.userAccounts.length <= 0 &&
						<div style={{textAlign:'center',color:'#e1e1e1'}}>Для входа в игру создайте свой первый аккаунт</div>
					}
					<div className="block__body">
						<div className="container no-gutters">
							{this.state.userAccounts.map(userAccount => {
								const process = this.state.userAccountProcess[userAccount.id] || {};
								return (<div className="row" key={userAccount.id}>
									<div className="col">
										<div className={cx("block__row", {
											"is_premium" : userAccount.bGM || userAccount.bPremium
										})}>
											<div className="name">
												{userAccount.bGM === true &&<span>[GM]&nbsp;</span>}
												{userAccount.name}
											</div>
											<div className="right">
												<button type="button"
													disabled={
														userAccount.isBanned
														|| process.running
														|| process.execution
														|| !this.props.isConnected
														|| !userAccount.bCreated
													}
													className={cx({
														"btn btn-sm" : true,
														"btn-danger" : userAccount.isBanned || !userAccount.bCreated,
														"btn-success" : !userAccount.isBanned && userAccount.bCreated,
														"btn-light" : !userAccount.isBanned && !userAccount.bCreated && userAccount.createFlag === 1,
														"disabled" : userAccount.isBanned || process.running || process.execution || !this.props.isConnected || !userAccount.bCreated
													})}
													onClick={this.handleOnClickLaunch.bind(null,userAccount)}>
														{process.running &&
															<i className="fa fa-spinner fa-pulse fa-fw"/>
														}
														{process.execution &&
															<i className="fa fa-check fa-fw"/>
														}
														{!process.execution
															&& !process.running &&
															<span>
																{userAccount.isBanned &&
																	<span>Banned</span>
																}
																{userAccount.bCreated && !userAccount.isBanned &&
																	<span>Play</span>
																}
																{!userAccount.isBanned && !userAccount.bCreated && userAccount.createFlag === 1 &&
																	<i className="fa fa-spinner fa-pulse fa-fw"/>
																}
																{!userAccount.isBanned && !userAccount.bCreated && (userAccount.createFlag === 2 || userAccount.createFlag === 4) &&
																	<i className="fa fa-times fa-fw"/>
																}
															</span>
														}
													</button>
											</div>
										</div>
									</div>
								</div>)
							})}
						</div>
					</div>

					{this.state.userAccounts.length < 70 && !this.state.loading && !this.state.error &&
						<button
							type="button"
							style={{margin:15}}
							onClick={this.onClickCreate}
							className={cx({
								"btn btn-info btn-sm" : true,
							})}><i className="fa fa-fw fa-plus"/> Добавить аккаут</button>
					}

					{!this.state.loading && !this.state.error &&
						<button
							type="button"
							style={{margin:'15px 5px'}}
							onClick={this.onClickLogout}
							className={cx({
								"btn btn-light btn-sm" : true,
							})}><i className="fa fa-fw fa-times"/> Войти с другой учетки</button>
					}

					{this.state.userAccounts.length >= 70 &&
						<div style={{padding:15,fontSize:12}}>Достигнут лимит 70 аккаунтов на пользователя. Прости, но ты оказался лютым твинководом, добавить еще не получится.</div>
					}
				</div>
			</div>
		);
	}
}

export default connect(
	(state, ownProps) => ({
		authUser : state.auth.ur,
		isConnected : state.connect.connected
	})
)(Component);