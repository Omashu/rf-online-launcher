import React from 'react';
import { connect } from 'react-redux';
import { SubmissionError } from 'redux-form';
import cx from 'classnames';
import Promise from 'bluebird';
import _ from 'lodash';

import { write as writeStorage } from '../../../storage';
import { setToken, reconnect } from '../../../socket';
import { getToken, activateToken, userAccountsIndexByUserId, userAccountsCreate } from '../../../action';

import FormAccountCreate from '../../forms/AccountCreate.jsx';

class Component extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
		};
	}
	componentWillMount() {
	}
	handleSubmit = (values) => {
		return userAccountsCreate({...values, userId : this.props.authUser.id})
			.then(data => {
				if (data.error)
					throw new SubmissionError({_error:data.error + ` (${data.code})`});

				return this.props.onClickCancel && this.props.onClickCancel();
			});
	}
	render() {
		return (
			<div className="sidebar__accountCreate">
				<div className="block block__accounts">
					<h1 className="header">
						Укажите желаемый логин
					</h1>
					<div className="block__body" style={{padding:'0 15px'}}>
						<FormAccountCreate
							onClickCancel={this.props.onClickCancel}
							onSubmit={this.handleSubmit}
							form="sidebarAccountCreate"/>
					</div>
				</div>
			</div>
		);
	}
}

export default connect(
	(state, ownProps) => ({
		authUser : state.auth.ur,
		connectionUrl : state.storage.connectionUrl,
		isConnected : state.connect.connected
	})
)(Component);