import _ from 'lodash'
import fs from 'fs'

let current = {};

export const get = () => {
	return current || {};
};

export const setCurrent = (object = {}) => {
	current = _.extend({}, object);
};

export const write = (object = {}) => {
	fs.writeFile('LauncherStorage.json', JSON.stringify(object, null, '\t'), {flag : 'w+'}, (err) => {
		if (err)
			return console.log(err);

		current = _.extend({}, object);
	});
};