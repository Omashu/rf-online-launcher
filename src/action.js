import Promise from 'bluebird';
import socket from './socket';

const emitWrapper = (eventName, data) => {
	return new Promise((resolve, reject) => {
		let tmOut = false;

		const timeout = setTimeout(() => {
			tmOut = true;
			return reject(new Error("Сервис недоступен или перегружен"));
		}, 10000);

		const handle = (res) => {
			// ответ пришел, но мы уже сказали что время вышло
			if (tmOut) return;
			clearTimeout(timeout);
			return resolve(res);
		};

		socket().emit(eventName, data, handle);
	});
};

export const getToken = (values) => {
	return emitWrapper("api_getToken", values);
};

export const activateToken = (values) => {
	return emitWrapper("api_activateToken", values);
};

export const userAccountsIndexByUserId = (userId) => {
	return emitWrapper("api_userAccounts_indexByUserId", userId);
};

export const userAccountsCreate = (values) => {
	return emitWrapper("api_userAccounts_create", values);
};

export const pageIndex = (values) => {
	return emitWrapper("api_pages_index", values);
};

export const createSession = (values) => {
	return emitWrapper("api_createSession", values);
};