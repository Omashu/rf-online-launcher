import * as types from '../types';

const initialState = {
	audioAutoPlay : true,
	userAccounts  : []
};

export default (state = initialState, action) => {

	if (action.type === types.STORAGE_AUDIOAUTOPLAY) {
		state = {
			...state,
			audioAutoPlay : action.payload === undefined ? true : !!action.payload
		}
	}

	if (action.type === types.STORAGE_USERACCOUNTS) {
		state = {
			...state,
			userAccounts : action.payload || []
		}
	}

	return state;
}