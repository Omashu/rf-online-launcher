import * as types from '../types';
import _ from 'lodash'

const initialState = {};

export default (state = initialState, action) => {

	if (action.type === types.SERVERDISPLAY_NEWSTATE)
		state = _.extend({}, action.payload);

	return state;
}