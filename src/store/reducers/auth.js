import * as types from '../types';

const initialState = {
	ur : null
};

export default (state = initialState, action) => {

	if (action.type === types.AUTH_UR) {
		state = {
			...state,
			ur : action.payload
		}
	}

	return state;
}