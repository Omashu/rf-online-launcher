import * as types from '../types';

const initialState = {
	loading : true
};

export default (state = initialState, action) => {
	if (action.type === types.APP_LOADING) {
		state = {
			...state,
			loading : !!action.payload
		};
	}

	return state;
}