import * as types from '../types';

const initialState = {
	connection : true,
	connected : false,
	lastECode : null
};

export default (state = initialState, action) => {

	if (action.type === types.CONNECT_CONNECTION) {
		state = {
			...state,
			connection : !!action.payload
		};
	}

	if (action.type === types.CONNECT_CONNECTED) {
		state = {
			...state,
			connected : !!action.payload
		};
	}

	if (action.type === types.CONNECT_LASTECODE) {
		state = {
			...state,
			lastECode : action.payload
		};
	}

	return state;
}