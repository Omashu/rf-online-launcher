import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form';

import app from './app'
import connect from './connect'
import storage from './storage'
import auth from './auth'
import serverDisplay from './serverDisplay'

export default combineReducers({
	app,
	connect,
	storage,
	auth,
	serverDisplay,
	form : formReducer
})