import io from 'socket.io-client';
import * as dispatchTypes from './store/types';

let Configured = false;
let Connection;
let Dispatch;
let Token;

const handleConnect = () => {
	// console.log("connect");
};

const handleEventServerLogin = (data) => {
	Dispatch({
		type : dispatchTypes.CONNECT_CONNECTION,
		payload : data.bConnection
	});
	Dispatch({
		type : dispatchTypes.CONNECT_CONNECTED,
		payload : data.bConnected
	});
	Dispatch({
		type : dispatchTypes.CONNECT_LASTECODE,
		payload : data.lastECode
	});
};

const handleEventServerDisplay = (data) => {
	Dispatch({
		type : dispatchTypes.SERVERDISPLAY_NEWSTATE,
		payload : data
	});
};

export const setToken = (token) => {
	Token = token;
};

export const reconnect = (url, options, dispatch) => {
	Configured = false;
	return connect(url, options, dispatch);
};

export const connect = (url, options = {}, dispatch) => {
	if (Configured)
		return Connection;

	Configured = true;
	Dispatch = dispatch;

	if (Connection) Connection.disconnect();

	options.query = options.query || {};
	options.query.token = Token;

	Connection = io(url, options);
	Connection.on("connect", handleConnect);
	
	Connection.on("ur", (data) => {
		Dispatch({
			type : dispatchTypes.AUTH_UR,
			payload : data
		})
	});

	Connection.on("serverLogin__haveNewState", handleEventServerLogin);
	Connection.on("serverDisplay__haveNewState", handleEventServerDisplay);

	return Connection;
};

export default connect;